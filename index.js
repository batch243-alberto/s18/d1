console.log("Good Aftie")
// Functions
	// functions in javascript are lines/blocks of codes that tell our devices/application to perform a certain task when called/inboke
	// functions are moslty created to create complicated tasks to run several lines of codes in succession. 
	// They are also used to prevrnt repeatng lines/blocks of codes that contains the same task functions.

	// We also learned in the previous session that we can gather data from user using prompt window.

		// function printInput(){
		//  	let nickname = prompt("Enter your nickname:");
		//  	console.log("Hi " + nickname)
		// }

		// printInput();

	// However, for some use cases, this may not be ideal
	// For other cases, functions can also process data directly passed into it instead of relying in Global Variable and prompt
	// Parameters and Arguments

// Parameters and Arguments

		// Consider this function/
		// "name" is called parameter which acts as named variable/container that exist only inside of a function
		function printName(name = "noName"){
			console.log("My name is " + name);
		}

		// "Chris" is the argument, the information/ data provided directly into the function
		printName("Chris");
		printName();

		// You can directly pass data into the function. The function can then use that data which is referred as "name" within the function
		// Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.  

	// variables can also ve passed as an argument. 
		let sampleVariable = "Edward";
		printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided within the function. 

		function noParams(){
			let params = "No parameter";
			console.log(params);
		}

		noParams("With parameter!");

		function checkDivisibilityBy8(num){
			let modulo = num%8;
			console.log("The remainder of " + num + " divided by 8 is: " + modulo);
			let isDivisibleBy8 = modulo === 0; 
			console.log("Is " + num + " divisible by 8? ");
			console.log(isDivisibleBy8);
		}

		checkDivisibilityBy8(8);

		checkDivisibilityBy8(17);

		// You can also do the same using the prompt (), however, take not that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Arguments
		// function paremeters can also accept other function as arguments. 
		// Some complex function use other functions as arguments to perform more complicated results/
		// This will be further seen when array methods were introduced

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed. ");
		}

		function argumentFunctionTwo(){
			console.log("Ths funtion was passed as an argument from the second argument function.")
		}

		function invokeFunction(argFunction){
			argFunction();
			// console.log(argFunction)
		}

		invokeFunction(argumentFunction);

		invokeFunction(argumentFunctionTwo);

		// Adding and removing the parentheses "()" impacts the output of JavaScript heavily. 
		// When function is used with parentheses "()", ut denotes invoking a function.
		// A function used w/o parenthesis "()" is normally associated with using the function as an argment to another function.

	// Using Multiple parameters
		// Multiple "arguments" will corresnpond to the number of "parementers" declared in a function in "succeeding order".


	// Succeeding Order
	function createFullName(firstName, middleName, lastName){
		console.log("This is firstName: " + firstName);
		console.log("This is middleName: " + middleName);
		console.log("This is lastName: " + lastName);
	}

	createFullName("Juan", "Dela", "Cruz");

	// "Juan" will be stored in the paremeter "firstName"
	// "Dela" will be stored in the paremeter "middleName"
	// "Cruz" will be stored in the paremeter "lastName"

	// In JavaScript, providing more arguments than the expected parameters will not return an error. 
	createFullName("Juan", "Dela", "Cruz", "Jr.");
	createFullName("Juan", undefined ,"Dela");

	// using variables as arguments/

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

// return statement
	// The "return" statement allows us to output a value from a function to be passed to the line/ block of code that invoked the function. 

	function returnFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName)
	}  

	returnFullName("Ada", "None", "Lovelace");

	// return makes the function a variable
	function returnName(firstName, middleName, lastName){
		return firstName + " " + middleName + " " + lastName;

		// all statements after the return will not be read
	}

	// returnName has contained a data, the function became a container
	console.log(returnName("John", "Doe", "Smith"));

	let fullName = returnName("John", "Doe", "Smith");
	console.log("this is the console.log from the fullName variable")
	console.log(fullName);

	function printPlayerInfo(userName, level, job){
		console.log("UserName: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);

		return userName + " " + level + " " + job;
	}

	printPlayerInfo("knight_white", 95, "Paladin");
	//  if return is used it will be contained in one variable

	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1);